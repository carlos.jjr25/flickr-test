//
//  DetailViewController.swift
//  FlickrTest
//
//  Created by Carlos Jimenez on 13/06/17.
//  Copyright © 2017 MobileMx. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIWebViewDelegate {
    
    var selectedItem : FlickrItem!
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = selectedItem.title
        self.webView.loadHTMLString((selectedItem?.descriptionFlickr)!, baseURL: nil)
        self.loadingView.startAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.loadingView.stopAnimating()
    }

}
