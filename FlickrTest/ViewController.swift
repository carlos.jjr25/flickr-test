//
//  ViewController.swift
//  FlickrTest
//
//  Created by Carlos Jimenez on 13/06/17.
//  Copyright © 2017 MobileMx. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class ViewController: UITableViewController {
    
    var items = [FlickrItem]()
    var clickedItem: FlickrItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        sendFlickrRequest()
        getItemsFromMemory()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getItemsFromMemory() {
        
        items.removeAll()
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PhotoItem")
        let dateSortDescriptor = NSSortDescriptor(key: "date_taken", ascending: false)
        fetchRequest.sortDescriptors = [dateSortDescriptor]
        
        do {
            let results =
                try managedContext.fetch(fetchRequest)
            items = results as! [FlickrItem]
            debugPrint("Found \(items.count) items in database")
            tableView.reloadData()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    private func sendFlickrRequest() {
        
        // Add URL parameters
        let urlParams = [
            "tags":"soccer",
            "format":"json",
            "nojsoncallback":"1",
        ]
        
        // Fetch Request
        Alamofire.request("http://www.flickr.com/services/feeds/photos_public.gne", method: .get, parameters: urlParams, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    //debugPrint("HTTP Response Body: \(response.result.value)")
                    
                    if let data = response.result.value as? [String : Any] {
                        self.parseItems(fromJSON: data["items"] as! [[String : Any]])
                    } else {
                        debugPrint("Failed to parse")
                    }
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error)")
                }
        }
    }
    
    private func parseItems(fromJSON json : [[String : Any]]) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        for item in json {
            
            let flickrItem = NSEntityDescription.insertNewObject(forEntityName: "PhotoItem", into: managedContext) as! FlickrItem

            flickrItem.title = item["title"] as? String
            flickrItem.descriptionFlickr = item["description"] as? String
            flickrItem.link = item["link"] as? String
            flickrItem.author = item["author"] as? String
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-ddTHH:mm:ssZZZ"
            flickrItem.date_taken = formatter.date(from: (item["date_taken"] as? String)!)
            
            
            let imageUrl = (item["media"] as! [String : Any])["m"] as! String
            
            let url = URL(string: imageUrl)!
            let data = try? Data(contentsOf: url)
            
            if let imageData = data {
                flickrItem.imageData = imageData
            }
            
            if checkForExistance(withKey: (item["link"] as? String)!) {
                // Item is already in database
                debugPrint("Already in database")
            } else {
                do {
                    try managedContext.save()
                    items.append(flickrItem)
                    debugPrint("Adding new item \(flickrItem.title!) to list")
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            }
        }
        self.tableView.reloadData()
    }
    
    private func checkForExistance(withKey key : String) -> Bool {
        
        
        for item in items {
            if item.link == key {
                return true
            }
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoCell", for: indexPath)
        let item = items[indexPath.row]
        
        cell.textLabel?.text = item.title
        cell.detailTextLabel?.text = item.author
        cell.imageView?.image = UIImage(data: item.imageData!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let item = items[indexPath.row]
        clickedItem = item
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailView = segue.destination as? DetailViewController {
            detailView.selectedItem = clickedItem
        }
    }

}

