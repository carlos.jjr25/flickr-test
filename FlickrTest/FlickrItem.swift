//
//  FlickrItem.swift
//  FlickrTest
//
//  Created by Carlos Jimenez on 13/06/17.
//  Copyright © 2017 MobileMx. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class FlickrItem: NSManagedObject {
    
    @NSManaged var title: String?
    @NSManaged var author: String?
    @NSManaged var link: String?
    @NSManaged var descriptionFlickr: String?
    @NSManaged var date_taken: Date?
    @NSManaged var imageData: Data?
}
